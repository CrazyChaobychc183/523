package com.nxd.binjiang.service.impl.system;

import com.nxd.binjiang.dao.system.RoleDao;
import com.nxd.binjiang.entity.system.Role;
import com.nxd.binjiang.service.RoleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.sql.SQLException;
import java.util.List;

/**
 * RoleServiceImpl
 *
 * @author OldMountain
 * @date 2017/12/4
 */
@Service
public class RoleServiceImpl implements RoleService {

    @Autowired
    private RoleDao roleDao;

    @Override
    public Role getRoleById(Role role) throws SQLException {
        Role role1 = new Role();
        role1.setRoleId(role.getRoleId());
        return roleDao.select(role1).get(0);
    }

    @Override
    public List<Role> listAll() throws SQLException {
        return roleDao.select(new Role());
    }

    @Override
    public List<Role> list(Role role) throws SQLException {
        return roleDao.select(role);
    }

    @Override
    public List<Role> getRoleListByParentId(Role role) throws SQLException {
        return roleDao.select(new Role(role.getParentId()));
    }

    @Override
    public int modify(Role role) throws SQLException {
        return roleDao.update(role);
    }

    @Override
    public int save(Role role) throws SQLException {
        return roleDao.insert(role);
    }

    @Override
    public int remove(Role role) throws SQLException {
        return roleDao.delete(role.getRoleId());
    }


}
