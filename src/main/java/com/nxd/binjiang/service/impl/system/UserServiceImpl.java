package com.nxd.binjiang.service.impl.system;

import com.nxd.binjiang.dao.system.UserDao;
import com.nxd.binjiang.entity.system.User;
import com.nxd.binjiang.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.sql.SQLException;
import java.util.List;

/**
 * UserServiceImpl
 *
 * @author OldMountain
 * @date 2017/12/4
 */
@Service
public class UserServiceImpl implements UserService {

    @Autowired
    private UserDao userDao;

    @Override
    public List<User> list(User user) throws SQLException {
        return userDao.select(user);
    }

    @Override
    public int modify(User user) {
        return 0;
    }

    @Override
    public int save(User user) {
        return 0;
    }

    @Override
    public int remove(User user) {
        return 0;
    }

    @Override
    public User login(String userName, String password) throws SQLException {
        User user = new User();
        user.setUserName(userName);
        user.setPassword(password);
        return userDao.select(user).get(0);
    }
}
