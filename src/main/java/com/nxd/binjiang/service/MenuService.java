package com.nxd.binjiang.service;

import com.nxd.binjiang.entity.system.Menu;
import com.nxd.binjiang.entity.system.Role;
import org.springframework.stereotype.Repository;

import java.sql.SQLException;
import java.util.List;

/**
 * MenuService
 *
 * @author OldMountain
 * @date 2017/12/4
 */
@Repository
public interface MenuService {

    List<Menu> listAllMenu() throws SQLException;

    /**
     * 查询所有父级菜单
     * @return
     * @throws SQLException
     */
    List<Menu> listParentAll() throws SQLException;

    /**
     * 查询所有子菜单
     * @param menuId
     * @return
     * @throws SQLException
     */
    List<Menu> selectSubMenuByParentId(String menuId) throws SQLException;

    String listTreeMenu(Role role, int type) throws SQLException;
}
