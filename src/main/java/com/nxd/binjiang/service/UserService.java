package com.nxd.binjiang.service;

import com.nxd.binjiang.entity.system.User;
import org.springframework.stereotype.Repository;
import org.springframework.stereotype.Service;

import java.sql.SQLException;
import java.util.List;

/**
 * UserService
 *
 * @author OldMountain
 * @date 2017/12/4
 */
@Repository
public interface UserService {

    List<User> list(User user) throws SQLException;

    int modify(User user) throws SQLException;

    int save(User user) throws SQLException;

    int remove(User user) throws SQLException;

    User login(String userName,String password) throws SQLException;

}
