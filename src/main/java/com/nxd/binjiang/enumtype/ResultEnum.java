package com.nxd.binjiang.enumtype;

/**
 * 返回结果枚举
 * Created by XXL on 2016/5/24.
 */
public enum ResultEnum {



    /**
     * 200：成功
     */
    SUCCESS(200, "成功"),

    /**
     * ：未知错误
     */
    ERROR(500, "未知错误"),

    /**
     * ：空指针
     */
    ERROR_NULL(501, "空指针"),

    /**
     * ：类型错误
     */
    ERROR_CLASS(502, "类型错误"),

    /**
     * 503：参数错误
     */
    ERROR_PARAMETER(503, "参数错误"),

    /**
     * 504：系统错误
     */
    ERROR_SYSTEM(504, "系统错误"),

    /**
     * 505：服务器拒绝连接
     */
    ERROR_CONNECTION_DENIED(505, "服务器拒绝连接"),
    /**
     * ：逻辑错误
     */
    LOGICAL_ERROR(800, "逻辑错误"),

    /**
     * 805：验证用户名密码错误
     */
    ERROR_VERIFICATION(805, "验证用户名密码错误"),

    /**
     * 806：验证用户名密码错误
     */
    ERROR_EXISTENCE(806, "没找到该用户"),

    /**
     * 807：验证码不存在
     */
    ERROR_ISNOTEXISIENCECODE(807, "不存在验证码"),

    /**
     * 808：验证码不存在
     */
    ERROR_CODEOVERDUE(808, "验证码过期"),

    /**
     * 809：验证码错误
     */
    ERROR_CODEERROR(809, "验证码错误"),
    /**
     * 810：重复注册
     */
    ERROR_DUPLICATEREGISTRATION(810, "重复注册"),
    /**
     * 811：用户没有授权的分店
     */
    ERROR_NOTAUTHORIZEDBRANCH(811, "用户没有授权到该分店"),
    /**
     * 812：不存在该分店
     */
    ERROR_THEREISNOBRANCH(812, "不存在该分店"),

    /**
     * 813：不存在该房间
     */
    ERROR_ISNOROOM(813, "不存在该房间"),
    /**
     * 814：不存在该房间
     */
    ERROR_ISNOAREA(814, "不存在该区域"),

    /**
     * 815：该用户不存在密码
     */
    ERROR_ISNOPASSWORD(815, "该用户不存在密码"),

    /**
     * 816：不存在该场景
     */
    ERROR_ISNOSCENE(816, "不存在场景"),

    /**
     * 817：控制设备失败
     */
    ERROR_CONTROLFAILURE(817, "控制设备失败"),
    /**
     * 818：该房间没有硬件设备
     */
    ERROR_ISNOHARDWAREINFO(818, "该房间没有设备"),

    /**
     * 819：没有任何硬件场景
     */
    ERROR_ISNOHARDWARESCENARIO(819, "没有任何硬件场景"),


    /**
     * 820：没找到该房间的能耗记录
     */
    LOGICAL_ERROR_ISNO_ENERGY(820, "没找到该房间的能耗记录"),

    /**
     * 821：没能找到该设备信息
     */
    LOGICAL_ERROR_ISNO_DEVICE(821, "没能找到该设备信息"),

    /**
     * 822：设备不在线
     */
    LOGICAL_ERROR_EQUIPMENT_OFFLINE(822, "设备不在线"),

    /**
     * 823：没有该楼层信息
     */
    LOGICAL_ERROR_ISNO_FLOOR(823, "没有该楼层信息"),

    /**
     * 824：发送短信失败
     */
    LOGICAL_ERROR_SEND_MSG(824, "发送短信失败"),

    /**
     * 825：控制场景失败
     */
    LOGICAL_ERROR_CONTROL_SCENE(825, "控制场景失败"),

    /**
     * 826：该分店还没设置logo
     */
    LOGICAL_ERROR_ISNO_LOGO(826, "该分店还没设置logo"),

    /**
     * 827：该分店还没定制化背景图
     */
    LOGICAL_ERROR_ISNO_BGIMG(827, "该分店还没定制化背景图"),

    /**
     * 828：该房间没有服务
     */
    LOGICAL_ERROR_ISNO_SERVICE(828, "该房间没有服务"),

    /**
     * 829：服务是打开状态请勿重复操作
     */
    LOGICAL_ERROR_SERVICE_ISOPEN(829, "服务是打开状态请勿重复操作"),

    /**
     * 830：服务器传递的分店ID与分店本地ID不匹配
     */
    LOGICAL_ERROR_HOTEL_NOT_EXIT(830, "服务器传递的分店ID与分店本地ID不匹配"),

    /**
     * 831：服务器传递的分店ID与分店本地ID不匹配
     */
    LOGICAL_ERROR_HOTELID_NOT_EXIT(831, "分店ID不能为空"),

    /**
     * 832：分店没有与分店ID进行绑定
     */
    LOGICAL_HOTELID_NOT_BINDING(832, "分店没有与分店ID进行绑定"),

    /**
     * 833：没有符合条件的卡劵配置
     */
    LOGICAL_ERROR_ISNO_COUPON(833, "没有符合条件的卡劵配置"),

    ERROR_NONSUPPORT(834,"不支持此类型操作");
    /**
     * 编码
     */
    private Integer code;

    /**
     * 描述
     */
    private String desc;

    /**
     *
     * Creates a new instance of RoomStatusEnum.
     *
     * @param p_code
     *            编码
     * @param p_desc
     *            描述
     */
    private ResultEnum(Integer p_code, String p_desc) {
        this.code = p_code;
        this.desc = p_desc;
    }

    /**
     * code.
     *
     * @return code 取得
     */
    public Integer getCode() {
        return code;
    }

    /**
     * desc.
     *
     * @return desc 取得
     */
    public String getDesc() {
        return desc;
    }

    /**
     * 返回编码.
     *
     * @return 编码
     * @see Enum#toString()
     */
    @Override
    public String toString() {
        return String.valueOf(this.getCode());
//		return new Gson().toJson(this);
    }

    /**
     *
     * getEnumByCode:(根据编码找到对象). <br/>
     *
     * @param code
     *            枚举编码
     * @return 枚举对象
     */
    public static ResultEnum getEnumByCode(Integer code) {
        ResultEnum result = null;
        ResultEnum[] values = ResultEnum.values();
        for (ResultEnum value : values) {
            if (value.getCode().intValue()== code.intValue()) {
                result = value;
                break;
            }
        }
        return result;
    }
}
